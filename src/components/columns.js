export const COLUMNS = [
  {
    Header: "id",
    accessor: "id",
  },
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Location",
    accessor: "location",
  },
  {
    Header: "Battery",
    accessor: "battery",
  },
  {
    Header: "State",
    accessor: "state",
  },
  {
    Header: "Options",
  },
];

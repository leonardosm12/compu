import React from "react";
import PropTypes from "prop-types";
const colorData = [
  "critical",
  "red",
  "low",
  "orange",
  "medium",
  "yellow",
  "high",
  "green",
  "fire detected",
  "red",
  "fire suppressed",
  "yellow",
  "no activity",
  "green",
];
export const Table = ({
  tableData,
  headingColumns,
  title,
  breakOn = "medium",
}) => {
  let tableClass = "table-container__table";

  if (breakOn === "small") {
    tableClass += " table-container__table--break-sm";
  } else if (breakOn === "medium") {
    tableClass += " table-container__table--break-md";
  } else if (breakOn === "large") {
    tableClass += " table-container__table--break-lg";
  }

  const data = tableData.map((row, index) => {
    let rowData = [];
    let i = 0;
    //coloring
    for (const key in row) {
      let value = row[key];
      if (key === "battery" || "state") {
        for (var k = 0; k < colorData.length; k++) {
          console.log(colorData[k]);
          if (row[key] === colorData[k]) {
            value = (
              <div style={{ backgroundColor: colorData[k + 1] }}>
                {row[key]}
              </div>
            );
          }
        }
      }

      rowData.push({
        key: headingColumns[i],
        val: value,
      });
      i++;
    }
    //options column buttons
    rowData.push({
      key: "Options",
      val: (
        <button type="button" disabled={true}>
          Click Me!
        </button>
      ),
    });
    return (
      <tr key={index}>
        {rowData.map((data, index) => (
          <td key={index} data-heading={data.key}>
            {data.val}
          </td>
        ))}
      </tr>
    );
  });

  return (
    <div className="table-container">
      <div className="table-container__title">
        <h1>{title}</h1>
      </div>
      <table className={tableClass}>
        <thead>
          <tr>
            {headingColumns.map((col, index) => (
              <th key={index}>{col}</th>
            ))}
          </tr>
        </thead>
        <tbody>{data}</tbody>
      </table>
    </div>
  );
};

Table.propTypes = {
  tableData: PropTypes.arrayOf(PropTypes.object).isRequired,
  headingColumns: PropTypes.arrayOf(PropTypes.string).isRequired,
  title: PropTypes.string.isRequired,
  breakOn: PropTypes.oneOf(["small", "medium", "large"]),
};

export default Table;

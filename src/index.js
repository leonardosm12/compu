import React, { useMemo } from "react";
import ReactDOM from "react-dom";
import MOCK_DATA from "./components/mock.json";
import "./index.css";
//import { BasicTable } from "./components/BasicTable";
import { Table } from "./components/Table";

function App() {
  const data = useMemo(() => MOCK_DATA, []);
  return (
    <div className="App">
      <div>
        <Table
          tableData={data}
          headingColumns={[
            "#",
            "Name",
            "Location",
            "Battery",
            "State",
            "Options",
          ]}
          title="Firefighter Sensors"
        />
      </div>
    </div>
  );
}
export default App;
ReactDOM.render(<App />, document.getElementById("root"));
